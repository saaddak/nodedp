"use strict";
/* Описание: Основной модуль - точка входа приложения. Содержит маршруты, доступные пользователям при взаимодействии с
 * системой. Находится в связи с модулем обработки запросов и данных из базы routing. Имеет две категории доступа к
 * страницам и данным: для авторизованных пользователей и для пользователей, не прошедших авторизацию.
 * Входящие данные: параметры запроса, сформированного на стороне пользователя, либо же непосредственно запрос маршрута
 * для отрисовки той или иной страницы.
 * Исходящие данные: перенаправление на станицу ошибки/заглушку | обработанные результаты запроса к файлу с данными |
 * направление на требуемую пользователем страницу.
 */

// НЕОБХОДИМЫЕ ПАКЕТЫ И ПЕРЕМЕННЫЕ
//====================================================================================================================//
let express = require('express'); // библиотека для осуществления работы HTTP сервера
let session = require('express-session'); // библиотека для осуществления сеанса связи с пользователем
let passport = require('passport'); // библиотека для осуществления опознавания пользователя
let LocalStrategy = require('passport-local').Strategy; // библиотека с функционалом стратегий опознавания пользователя
let bodyParser = require('body-parser'); // библиотека для работы с get/post запросами в express
let createError = require('http-errors'); // библиотека для обработки ошибок HTTP-запросов
const PORT = 3000; // порт, открытый для прослушивания приложением
let app = express(); // создание экземпляра express
let jsonParser = bodyParser.json(); // создание разборника для данных в формате JSON
//====================================================================================================================//

// ОСНОВНАЯ ЛОГИКА
//====================================================================================================================//
let path = require('path');
let cookieParser = require('cookie-parser');

// указание на соответствующие прослойки маршрутов
let indexRouter = require('./routes/index'); // главная страница - точка входа
let loginRouter = require('./routes/login'); // страница входа в профиль
let logoutRouter = require('./routes/logout'); // выход из профиля
let profileRouter = require('./routes/profile'); // профиль пользователя
let booksRouter = require('./routes/books'); // страница с книгами
let authorsRouter = require('./routes/authors'); // страница с авторами
let libgetterRouter = require('./routes/getlibrary'); // получение библиотеки
let notfoundRouter = require('./routes/404error'); // страница 404
let forbiddenRouter = require('./routes/403error'); // страница 403

// настройка представлений
app.set('views', path.join(__dirname, 'views/pages'));
app.set('view engine', 'ejs');

// настройка маршрутизации express
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
/*--------------------------------------------------------------------------------------------------------------------*/

/* Описание: Настройка локальной стратегии опознавания пользователя. Первичное обращение к системе распознавания
* (вход в систему) осуществляется при помощи адреса электронной почты. Последующие обращения после распознавания
* пользователя осуществляются по идентификатору пользователя.
* Входящие данные: пользовательский запрос с данными.
* Исходящие данные: отклонение либо принятие пользователя в качестве опознанного.
*/
passport.use("local-login", new LocalStrategy({ // настройка параметров опознания пользователя
        usernameField: "user", // имя пользователя
        passwordField: "password", // пользовательский пароль
        passReqToCallback: true // доступ к объекту запроса
    },
    function(request, user, password, done) { // реализация стратегии опознавания
        // здесь должен быть запрос данных о пользователе к БД, но вместо этого - имитация
        if(user === 'Admin' && password === '123') { // проверка имитированных данных
            return done(null, {name: 'Admin'}); // теперь пользователь принят
        } else {
            return done(null, false); // пользователь не принят
        }
    }
));
/*--------------------------------------------------------------------------------------------------------------------*/

/* Описание: "Сериализация" пользователя. Производится при входе в систему.
 * Входящие данные: данные пользователя, принимаемые на опознавание.
 * Исходящие данные: данные пользователя, принятые при опознании в системе.
 */
passport.serializeUser(function(user, done) {
    done(null, user.name);
});
/*--------------------------------------------------------------------------------------------------------------------*/

/* Описание: "Десериализация" пользователя. Производится при входе в систему и каждый раз при запросе пользователя.
 * Входящие данные: идентификатор пользователя.
 * Исходящие данные: данные пользователя, принятые при опознании в системе.
 */
passport.deserializeUser(function(name, done) {
    UserDeserialize(name);
    function UserDeserialize(name) {
        if (name === "Admin") { // здесь должен быть запрос данных о пользователе, но вместо базы - имитация
            done(null, name);
        } else { // если не равно идентификатору пользователя
            done(null, false);
        }
    }
});
/*--------------------------------------------------------------------------------------------------------------------*/

/* Описание: Настройка параметров сессии с пользователем.
 * Входящие данные: -
 * Исходящие данные: -
 */
app.use(session({
    secret: "FlyMeToTheMoonLetMePlayAmongTheStars",
    resave: true,
    saveUninitialized: true,
}));
app.use(passport.initialize()); // инициализация пользователей
app.use(passport.session()); // сессии passport
/*--------------------------------------------------------------------------------------------------------------------*/

/* Описание: Функция проверки входа текущего пользователя в систему. Производится при запросе определённого защищаемого
 * содержимого.
 * Входящие данные: запрос пользователя; ответ сервера; функция обратного вызова.
 * Исходящие данные: функция обратного вызова - перенаправление на страницу ошибки.
 */
function isLoggedIn(request, response, next) {
    if(request.isAuthenticated()) { // если вход в систему произведён,
        return next(); // продолжать работу
    } else { // если нет,
        //next(createError(403)); // перенаправлять на страницу ошибки
        response.redirect('403error');
    }
}
/*--------------------------------------------------------------------------------------------------------------------*/

// использование дополнительной прослойки для маршрутов
app.use('/', indexRouter); // главная страница, доступна всем, дополнительная логика доступа - в прослойке
app.use('/login', loginRouter); // форма входа, доступна всем, дополнительная логика доступа - в прослойке
app.use('/logout', isLoggedIn, logoutRouter); // команда выхода, доступна вошедшим, логика доступа isLoggedIn
app.use('/profile', profileRouter); // страница профиля, доступна вошедшим, логика доступа - в прослойке
app.use('/books', isLoggedIn, booksRouter); // страница с книгами, доступна вошедшим, логика доступа isLoggedIn
app.use('/authors', isLoggedIn, authorsRouter); // страница с авторами, доступна вошедшим, логика доступа isLoggedIn
app.use('/getlibrary', isLoggedIn, libgetterRouter); // запрос библиотеки, доступен вошедшим, логика - в прослойке
app.use('/404error', notfoundRouter); // заглушка 404, доступна всем
app.use('/403error', forbiddenRouter); // заглушка 403, доступна всем
//====================================================================================================================//

// ОБРАБОТКА ОШИБОК
//====================================================================================================================//
// отлов ошибки 404 и перенаправление на обработчик ошибок
app.use(function(request, response, next) {
  //next(createError(404));
    response.redirect('404error');
});
/*--------------------------------------------------------------------------------------------------------------------*/

// обработчик ошибок
app.use(function(error, request, response, next) {
  // отладочная обработка ошибок
    response.locals.message = error.message;
    response.locals.error = request.app.get('env') === 'development' ? error : {};

  // отрисовка страницы ошибки
    response.status(error.status || 500);
    response.render('error');
});
//====================================================================================================================//

// ЗАПУСК СЕРВЕРА
//====================================================================================================================//
/* Описание: Запуск сервера с ранее заданными параметрами.
 * Входящие данные: порт, который прослушивает сервер.
 * Исходящие данные: определяются процессом работы сервера.
 */
app.listen(PORT, function() {
    console.log("HTTP-СЕРВЕР ЗАПУЩЕН. ПРОСЛУШИВАЕТСЯ ПОРТ:" + PORT + ".");
});
//====================================================================================================================//