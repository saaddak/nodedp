let express = require('express');
let router = express.Router();

router.get('/', function(request, response, next) {
    response.render('books', {title: 'Книги'});
});
module.exports = router;