let express = require('express');
let router = express.Router();

router.get('/', function(request, response, next) {
    if(request.isAuthenticated()) { // если вход в систему произведён,
        response.render('profile', {title: 'Профиль'}); // загружается страница профиля пользователя
    } else { // если вход не поизведён,
        response.render('login', {title: "Форма входа", message: ""}); // отправлять на страницу авторизации
    }
});
module.exports = router;
