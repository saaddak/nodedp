let express = require('express');
let router = express.Router();

router.get('/', function(request, response, next) {
    if(request.isAuthenticated()) { // если вход в систему произведён,
        response.render('profile', {title: 'Профиль'}); // загружается страница профиля пользователя
    } else { // если вход не поизведён,
        response.render('index', {title: 'Главная'}); // отправлять на главную страницу
    }

});

module.exports = router;
