let express = require('express');
let router = express.Router();

router.get('/', function(request, response, next) {
    response.render('authors', {title: 'Авторы'});
});
module.exports = router;