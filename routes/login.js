let express = require('express');
let passport = require('passport');
let router = express.Router();

router.get('/', function(request, response, next) {
    if(request.isAuthenticated()) { // если вход в систему произведён,
        response.render('profile', {title: "Профиль"}); // загружается страница профиля пользователя
    } else { // если вход не поизведён,
        response.render('login', {title: "Форма входа", message: ""}); // отправлять на страницу авторизации
    }
});
router.post('/', function(request, response, next) {
    //passport.authenticate('local-login', {successRedirect : "profile", failureRedirect: "login"}) // описание стратегии авторизации и перенаправлений
    passport.authenticate('local-login', function(error, user, info) {
       if(error) {
           return next(error);
       }
       if(!user) {
           return response.render('login', {title: "Форма входа", message: "failure"});
       }
       request.logIn(user, function(error) {
           if(error) {
               return next(error);
           }
           return response.render('profile', {title: "Профиль"})
       })
    })(request, response, next);
});

module.exports = router;