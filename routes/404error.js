let express = require('express');
let router = express.Router();

router.get('/', function(request, response, next) {
    if(request.isAuthenticated()) { // если вход в систему произведён,
        response.render('404errorauth', {title: 'Содержимое не найдено'});
    } else { // если вход не поизведён,
        response.render('404error', {title: 'Содержимое не найдено'});
    }

});
module.exports = router;