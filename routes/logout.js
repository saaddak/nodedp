let express = require('express');
let router = express.Router();

router.get('/', function(request, response, next) {
    request.logout();
    response.redirect('/'); // перенаправление на точку входа
});
module.exports = router;