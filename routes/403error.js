let express = require('express');
let router = express.Router();

router.get('/', function(request, response, next) {
    response.render('403error', {title: 'Доступ запрещён'});
});
module.exports = router;