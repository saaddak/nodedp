let express = require('express');
let fs = require('fs');
let router = express.Router();

router.get('/', function(request, response, next) {
    let errorMessage = {error: ""};
    fs.readFile("./views/libinfo.json", "utf-8", function (error, data) { // загрузка файла
        if (error) { // обработка ошибок открытия файла
            console.log("ОШИБКА ЗАГРУЗКИ ФАЙЛА.");
            if (error.code === "ENOENT") { // отработка в случае отсутствия файла
                errorMessage = {error: "ENOENT"};
                return response.send(errorMessage);
            } else { // прочие ошибки
                errorMessage = {error: "ERROR"};
                return response.send(errorMessage);
            }
        } else {
            let jsonParsed = JSON.parse(data); // загрузка файла в переменную и работа с ней
            let result = {data: "empty"};

            for(let i in jsonParsed.library) {
                if(jsonParsed.library.hasOwnProperty(i)) {
                    if(result.data === "empty") {
                        result = JSON.stringify(jsonParsed.library[i]);
                    } else {
                        result = result + JSON.stringify(jsonParsed.library[i]);
                    }
                }
            }
            response.send(data);
        }
    });
});
module.exports = router;